<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
  return;
}



?>

<tr class="tr-product">  
  <td class="code"> <?php $cod = $product->get_sku();   echo $cod;  ?> </td>
  <td class="nombre"> <label for="<?php  echo $cod ?>"> <?php echo get_the_title();   ?> </label> </td>
  <td class="unidad"> 
    <?php   
      $unidad = get_post_meta( $product->get_id(), 'unidad', true );
      $unidad = $unidad ? $unidad : 'UNI';
      echo $unidad;
    ?>  
  </td>

  <td class="cantidad">
    <div class="quantity buttons-added">
      <a href="javascript:void(0)" class="minus">-</a>
    <input type="number" id="<?php echo $cod; ?>" class="input-text qty text" step="1" min="0" max="" name="quantity" value="1" title="Cantidad" size="4" placeholder="" inputmode="numeric">
      <a href="javascript:void(0)" class="plus">+</a>
    </div>
  </td>

  <td  class="cotiza" data-id="<?php echo $product->get_id(); ?>">
    <?php echo do_shortcode( '[yith_ywraq_button_quote product="' .  $product->get_id()  . '"]'); ?>
  </td>
 </h1>
</tr>