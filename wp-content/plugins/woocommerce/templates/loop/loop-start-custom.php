<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$category = get_queried_object();
$className = get_class($category);
$isParent = $category->parent == 0;

?>



<?php if( $isParent ): ?>
  <ul class="products aceper columns-<?php echo esc_attr( wc_get_loop_prop( 'columns' ) ); ?>" data-id="woocomerce custom">

<?php else: ?>
  <div class="container-products-table" data-id="woocomerce custom">
  <h1 class="table-products-title">LISTADO DE PRODUCTOS A COTIZAR </h1>
  <table class="table-products">
  <thead>
      <th class="code"> Codigo  </th>
      <th class="nombre"> Nombre  </th>
      <th class="unidad"> Unidad  </th>
      <th class="cantidad"> Cantidad  </th>
      <th class="cotiza"> Cotiza </th>
  </thead>
  <tbody>
  <div>

<?php endif; ?>