<?php 
  $category = get_queried_object();
  $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
  $image = wp_get_attachment_url( $thumbnail_id ); 
  $isParent = $category->parent == 0;
?>


<!-- ####################### Categoria  ######################  -->

<div class='products-add <?php echo $isParent ? 'parent' : 'children';  ?>  grid-container'>

<?php  if( $isParent ): ?>

  <div class='subcategory-title'>
    <h2 class="product-subcategory"> SubCategorias </h2>
  </div>

<!-- ####################### Categoria  ######################  -->



<!-- ####################### Subcategoria ######################  -->

<?php else: ?>

  <?php
    $info = get_term_meta( $category->term_id, 'categoria_descripcion_corta', true );
    // dd($category);
  ?>

  <!-- Imagen -->
  <div class='section-img grid-25' data-info="<?php echo $info ; ?>">
    <div class="category-img">
      <img data-info="<?php echo $image; ?>" src='<?php echo $image; ?>' alt='Imagen categoria'/>
    </div>
    <div class="category-img-aviso"> *Las fotografías y descripciones son referenciales. </div>
    <div class="compartir">
    </div>
  </div>
  <!-- /Imagen -->

  <!-- Descripcion -->
  <div class='section-descripcion grid-50'>
    <h2 class="title"> Descripcion de los productos </h2>
    <div class="descripcion"> <?php echo $category->description; ?> </div> 
  </div>
  <!-- /Descripcion -->

  <!-- Aviso -->
  <div class='section-aviso grid-25'>
    <div class="aviso">
      <div class="title"> <span class="far fa-circle"> </span> AVISO </div>
      <div class="text">
        Los términos de venta principal para clientes del exterior son en loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor loremp ipsum odlor 
      </div>
      </div>
    </div>
  <!-- /Aviso -->


<?php endif; ?>

</div>