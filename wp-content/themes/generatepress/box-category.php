  
<?php

  $args = [
    'taxonomy' => 'product_cat',
    'orderby' => 'id',
    'order' => 'ASC',
    'parent' => '0',
    'hide_empty' => true
  ];


  $even = true;
  // Categorias principales
  $categorys = get_categories($args);
?>


<?php

  // Iterar categorias
  foreach( $categorys as $category ) :

  echo "<div class='shop-category-wrapper'>";
    echo "<div class='shop-category-inner grid-container'>";

    $className = $even ? 'even' : 'odd';
    $even = ! $even;
    $categoryLink =get_category_link($category->cat_ID);

      
    // Descripción ----------
    echo "<div class='section-name grid-100'>";
      echo sprintf("<div class='title'> <h2> <a href='%s'> %s </a> </h2> </div>", $categoryLink , $category->name );
    echo "</div>";
    // ----------

    // ---------- Subcategorias ----------

    // Iterar
      $args = [ 
        'taxonomy' => 'product_cat',
        'orderby' => 'id',
        'order' => 'ASC',        
        'parent' => $category->cat_ID,
        'hide_empty' => false
      ];

      // Subcategorias
      echo "<div class='section-subcategory grid-container'>";
      $index = 1;
      foreach( get_categories( $args ) as $subCategory ) :

        if( $index == 1 ){
          echo "<div class='grid-row section-row-4  grid-container'>";
        }


        $term_id = $subCategory->term_id;
        $thumbnail_id = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
        $img = wp_get_attachment_url( $thumbnail_id ); 
        // $img = wp_get_attachment_thumb_url( $thumbnail_id ); 
        // wp_get_attachment_thumb_url
        // <?php $wcatTerms = get_terms('product_cat', array('hide_empty' => 0, 'parent' =>0)); 
        // $categoria_descripcion_corta = get_woocommerce_term_meta($subCategory->cat_ID, 'categoria_descripcion_corta', true );        
        // $categoria_descripcion_corta = get_terms( 'product_cat', $subCategory->cat_ID, 'categoria_descripcion_corta', true );        

        $categoria_descripcion_corta = '';        
        // echo sprintf('<div class="grid-25 subcategory-box" style="background-image:url(%s)>', $img );
        echo sprintf('<div class="grid-20 subcategory-box" style="background-image:url('. "'%s'" . ')">', $img );
          echo sprintf('<a href="%s">' , get_category_link($subCategory->cat_ID) );          
            // echo sprintf('<div class="image-div"> <img src="%s"/> </div>', $img);
            echo sprintf('<div class="title-div"> <h3> %s </h3> <span class="descripcion">%s</span> </div>', $subCategory->name , $categoria_descripcion_corta );
          echo '</a>';
        echo "</div>";

        if( $index == 4 ){
          echo "</div>";
          $index = 1;
        }

        else {
          $index++;
        }


      endforeach;
      echo "</div>";


    // --------------------

    echo "</div>";
  echo "</div>";
  endforeach;