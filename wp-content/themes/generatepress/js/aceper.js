$(document).ready(function(){

  // Product Link  
  let $link = $("#link-show-categories");

  $link.on('click', function(e){
    //  fa-close
    e.preventDefault();

    let 
    $div_cat = $(".div_products_categories"),
    $icon = $("span.icono" , this),
    isHide = $div_cat.is('.hide'), 
    // classClose = "fa-window-close",
    classClose = "fa-caret-down",
    classOpen = "fa-caret-down";


    if( isHide ){

      $div_cat.removeClass('hide')
      $icon
      .addClass(classOpen)
      .removeClass(classClose)
    }

    else {
      $div_cat.addClass('hide')
      $icon
      .addClass(classClose)
      .removeClass(classOpen)
    }
  });



// --------------  Div emergente de busqueda de producto  --------------

 // Mostrar caja de busqueda de producto
 $("#searchProduct").on('click',  function(e){
  e.preventDefault();
  $(".widget_product_search").show();
  $(".widget_product_search form").show(300);
  $(".widget_product_search search-field").focus();  
 })

 // Ocultar y poner clase para darle los estilos
 $(".widget_product_search")
 .hide()
 .addClass('box-search-product');

 $(".widget_product_search").on('click' , 'form,input,button',  function(e){

    e.preventDefault();
    let searchValue = document.getElementById("woocommerce-product-search-field-0").value;

    let isForm = $(this).is('form')

    console.log( "isForm" , isForm , searchValue );

    if( $(this).is('form') ){
      $(  ".woocommerce-product-search" ).hide();
      return false;
    }


   if( $(this).is('input') ){
      return false;
    }

   if( $(this).is('button') && searchValue.length  ){
      $(".widget_product_search form").submit();
      return false;
    }

    return false;
 })




// --------------  Poner el id especifico en la caja de agregar al carrito  --------------
  let $meta = $("[rel=shortlink]");

  if( $meta.length ){

    let producto_id = $("[rel=shortlink]").attr('href').split('p=')[1];
    // let producto_id = link_producto[1];
    let producto_code = $(".sku_wrapper .sku").text()

    // Ponerle manualmente el product id
    $(".box-add-product-single [data-product_id]").each(function(index,dom){

      let $this = $(dom);
      $this
      .attr('data-product_id' , producto_id)
      .data('product_id' , producto_id);
    })

    // Ponerle manualmente codigo del producto al id del input
    $(".box-add-product-single input.qty").each(function(index,dom){
      let $this = $(dom).attr('id',producto_code)
    })

    // Ponerle manualmente la clase
    $(".box-add-product-single .ele-with-class-product-id").each(function(index,dom){
      let $this = $(dom);
      let oldClass = $this.attr('class');
      let newClass = oldClass.replace('%product_id%' , producto_id);
      $this.attr('class' , newClass)
    })

  }

})


