<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_HOME','http://localhost/aceper_');
define('WP_SITEURL','http://localhost/aceper_');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'aceper_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.DF2d2*n]7<W5JQ[&ws$`95Wx:,/2QUsg!S9^]7exICBSHJ=c~WNXirIXR )Rifj' );
define( 'SECURE_AUTH_KEY',  '2*J&%p.]C~-!AF|Ky%u3>sZ{& OP8B!Tv:IaZ~<)-7U+c//Zy]9GRwb{j!257~TS' );
define( 'LOGGED_IN_KEY',    'T&+hXLo1JZr90WeGvaI>X.Jl4B~H63GOG1F`[Xsy!;T cHP1%Yg43<#jweeOj1(K' );
define( 'NONCE_KEY',        '%:%]n|+m) +G)GF_Uv>P%*Vo0Uksg&NcF%u7NkOvZ1Qz,zM-aJfTOi=$xIO nw8A' );
define( 'AUTH_SALT',        'u%rE:H,6O*uQ8n>?0iCQwd-Kfa{MwK<rH@%|p[g+emfUxf@oxV2UcBns=+>:f/~:' );
define( 'SECURE_AUTH_SALT', '/f^llBl&-kf v4o!9[<{v_^rGYCCQXIBDx&l2,$tzkVp#!6n. bkaZ ?)1/(Q)Pj' );
define( 'LOGGED_IN_SALT',   '/)XHzx(A4~vll[[sQc <kc]VcxG*n%:c>XbW<@:uCsE2!WHbSJ6:>Npxo<,Z#(jP' );
define( 'NONCE_SALT',       '$l%gB:U=$x00QpH!B| kci82}5F5AE307vq1=jx+$)Px(3xlIn`G=_pV}*6^jDGy' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors',0);
define('WP_DEBUG_LOG', false );
define('WP_DEBUG_DISPLAY', false);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
